require('dotenv').config();
const express = require('express');
const morgan = require('morgan');
const routes = require('./routes');

const app = express();

app.set('view engine', 'ejs');
app.use(morgan('dev'));
app.use(express.json());
app.use(routes)

app.use((err, _req, res, _next) => {
  res.render('500', { error: err.message });
});

app.use((_req, res, _next) => {
  res.render('404');
});

app.listen(8080, () => {
  console.log('Server running on port 8080');
});